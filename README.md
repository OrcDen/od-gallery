# <od-gallery>

> A component to organize and display thumbnails of images. Has two modes.

`<od-gallery>` is a housing component for `<img>` tags. Depending on the mode of operation is will display them in a grid or a simple carousel with multiple thumnails. when a thumbnail is clicked a modal is shown with alrger size image.

## Installation
- Install with [npm](https://www.npmjs.com/)

```
npm i @orcden/od-gallery
```
## Usage
```
import '@orcden/od-gallery';
```
```
<od-gallery id="od-gallery" carousel>
    <img src="https://images.unsplash.com/photo-1569409611407-50eee9f59dfe?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1389&q=80">
    <img src="https://images.unsplash.com/photo-1567815883115-bcf719bdc8ad?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1351&q=80">
    <img src="https://images.unsplash.com/photo-1568121581570-a30e94219113?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80">
    <img src="https://images.unsplash.com/photo-1568387188834-ff83d00b9915?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80">
    <img src="https://images.unsplash.com/photo-1550728193-be87c574be86?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80">
</od-gallery>
<od-gallery id="od-gallery">
    <img src="https://images.unsplash.com/photo-1569409611407-50eee9f59dfe?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1389&q=80">
    <img src="https://images.unsplash.com/photo-1567815883115-bcf719bdc8ad?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1351&q=80">
    <img src="https://images.unsplash.com/photo-1568121581570-a30e94219113?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80">
    <img src="https://images.unsplash.com/photo-1568387188834-ff83d00b9915?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80">
    <img src="https://images.unsplash.com/photo-1550728193-be87c574be86?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80">
</od-gallery>
```
    
## Attributes
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `thumbnail-width`  | Number  | 80       | In pixels. Set to change how wide the thumnails are. Thumbnails are always 16:9 aspect  |
| `carousel`  | Boolean | false   | Set this to tell the gallery to display as a carousel of thumbnails |

## Properties
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `thumbnailWidth`  | Number  | 80       | In pixels. Set to change how wide the thumnails are. Thumbnails are always 16:9 aspect  |
| `carousel`  | Boolean | false   | Set this to tell the gallery to display as a carousel of thumbnails |


## Styling
- CSS variables are available to alter the default styling provided

| Shadow Parts     | Description           |
|------------------|-----------------------|
| view-grid           | When not in carousel mode this div houses and arranges the rows of thumnails |
| view-carousel           | When in carousel mode this is the main carousel that houses and arranges the rows of thumnails |
| view-carousel-inner-container    | When in carousel mode this is the exposed part of the carousel container |
| view-carousel-prev-button     | When in carousel mode this is the exposed part of the carousel previous button |
| view-carousel-prev-button     | When in carousel mode this is the exposed part of the carousel next button |
| modal    | This is the main modal component that displays after a thumbnail is clicked |
| modal-inner   | This is exposed part of the modal container that does most of the modal work |
| modal-carousel-contaier   | A simple div to hold the modal carousel |
| modal-carousel           | This is the main carousel component that houses the larger images after a thumb is clicked |
| modal-carousel-inner-container    | The exposed part of the modal carousel container |
| modal-carousel-prev-button     | The exposed part of the modal carousel previous button |
| modal-carousel-prev-button     | The exposed part of the modal carousel next button |


## Development
### Run development server and show demo

```
npm run demo
```

### Run linter

```
npm run lint
```

### Fix linter errors

```
npm run fix
```

### Run tests

```
npm run test
```

### Build for production

```
npm run build
```