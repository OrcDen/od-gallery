import '@orcden/od-carousel';
import '@orcden/od-modal';

const template = document.createElement( "template" );
template.innerHTML = `
    <style>
        :host {
            display: block;
            font-size: inherit;
            border: solid 1px black;
            box-sizing: border-box;
            padding: 0 !mportant;
        }

        :host([carousel]) {
            height: 100px;
        }

        :host(:not([carousel])) #view-carousel {
            display: none;
        }

        :host([carousel]) #view-grid {
            display: none;
        }

        #content::slotted(*) {
            display: none;
        }

        #content-container {
            display: flex;
            flex-wrap: wrap;
        }

        .od-gallery-thumbnail {
            display: flex;
            justify-content: center;
            align-items: center;
            padding: .5em;
            box-sizing: border-box;
            cursor: pointer;
        }

        .thumbnail-group {
            display: flex;
            justify-content: center;
            align-items: center;
            padding: .5em;
            height: 100%;
            box-sizing: border-box;
            margin: 0 !important
        }

        #modal-carousel-container {
            display: block;
            height: 100%;
            width: 100%;
        }

        #modal-carousel {
            height: 100%;
            width: 100%;
        }

        #modal-carousel::part( container ) {
            background-color: white;
        }

    </style>

    <div id="content-container">
        <slot id="content"></slot>
    </div>
    
    <div id='view-grid' part='view-grid'></div>

    <od-modal id="modal" part='modal' exportparts='modal : modal-inner'>
        <div id="modal-carousel-container" part='modal-carousel-container'>
            <od-carousel id="modal-carousel" part='modal-carousel' exportparts='container : modal-carousel-inner-container, prev-button : modal-carousel-prev-button, next-button : modal-carousel-next-button'></od-carousel>
        </div>
    </od-modal>   
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-gallery" );

export class OdGallery extends HTMLElement {
    
    constructor() {
        super();
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {
        if( !this.getAttribute( 'thumbnail-width' ) ) {
            this.setAttribute( 'thumbnail-width', 80 );
        }
        //all properties should be upgraded to allow lazy functionality
        this._upgradeProperty( "carousel" );
        this._upgradeProperty( "thumbnail-width" );

        this.thumbAspect = 0.5625;
        this._images = [];
        this._thumbs = [];
        this._groups = [];
        this._groupMinWidth = null;
        this._groupMaxWidth = null;
        this._initializeImages();
        this._createGroups();
        this._attachGroups();

        this._addResizeListener();
        this.addEventListener( 'od-gallery-show-modal', this._showModal );    
        
        this._reloadGallery();
    }

    disconnectedCallback() {}


    //General - every element should have this
    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ["carousel"];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        switch ( attrName ) {
            case "carousel": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setCarouselAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setCarouselAttribute( newValue );
                }
                break;
            }
            case "thumbnail-width": {
                if (!this._validateNumber( newValue ) ) {
                    this._setThumbAttribute( oldValue );
                }
                break;
            }
        }
    }
    
    get carousel() {
        return this.hasAttribute( "carousel" );
    }

    set carousel( isCarousel ) {
        if ( typeof isActive !== "boolean" ) {
            return;
        }
        this._setCarouselAttribute( isCarousel );
    }

    _setCarouselAttribute( newV ) {        
        this._setBooleanAttribute( 'carousel', newV );
    }

    _validateBoolean( newV ) {
        return (
            typeof newV === "boolean" || newV === "true" || newV === "false"
        );
    }

    _setBooleanAttribute( name, newV ) {
        if ( newV !== "" && ( !newV || newV === "false" ) ) {
            this.removeAttribute( name );
        } else {
            this.setAttribute( name, true );
        }
    }

    get thumbnailWidth() {
        return Number.parseInt( this.getAttribute( "thumbnail-width" ) );
    }

    set thumbnailWidth( width ) {
        if ( typeof width !== "number" ) {
            return;
        }
        if ( this._validateNumber( width ) ) {
            this.setAttribute( "offset", width );
        }
    }

    _validateNumber( newV ) {
        let temp = parseInt( newV );
        return !isNaN( temp ) && temp >= 0;
    }

    //Private
    _addResizeListener() {
        var ro = new ResizeObserver( entries => {
            for ( let entry of entries ) {
                if( this.offsetWidth < this._groupMinWidth || this.offsetWidth > this._groupMaxWidth ) {
                    this._reloadGallery();
                }
            }
        } );
        ro.observe( this );                
    }

    _initializeImages() {
        var images = this.shadowRoot
            .querySelector( '#content' )
            .assignedNodes( {
                flatten: true
            } )
            .filter( n => n.nodeType === Node.ELEMENT_NODE && n.tagName.toLowerCase() === 'img' );
        
        if( images.length < 1 ) {
            return;
        }       
        
        let modalCarousel = this.shadowRoot.querySelector( '#modal-carousel' );
        for( var i in images ) {
            var image = images[i];
            
            let imageAspect = image.offsetHeight / image.offsetWidth;
            if( imageAspect > this.thumbAspect ) {
                image.style.height = '100%';
                image.style.width = 'auto';
            } else {                
                image.style.height = 'auto';
                image.style.width = '100%';
            }

            var cloneImage = image.cloneNode( true );
            modalCarousel.appendChild( cloneImage )
            this._images.push( cloneImage );

            var thumb = document.createElement( 'div' );
            thumb.classList.add( 'od-gallery-thumbnail' );
            thumb.style.display = 'none';
            thumb.style.width = this.thumbnailWidth + 'px';
            thumb.addEventListener( 'click', ( e ) => {
                var dataSrc = e.target.getAttribute( 'src' );
                var event = new CustomEvent( 'od-gallery-show-modal', {
                    bubbles: true,
                    composed: true,
                    'detail': {
                        'dataSrc': dataSrc
                    }
                } );
                this.dispatchEvent( event );
            } );

            thumb.appendChild( image );
            this._thumbs.push( thumb );               
            thumb.style.display = 'flex';
        }
    }

    _createGroups() {
        if ( !this._thumbs || this._thumbs.length < 1 ) {
            return;
        }

        let thumbsInGroup = Math.floor( this.offsetWidth / this.thumbnailWidth );        
        
        if( thumbsInGroup < 1 ) {
            thumbsInGroup = 1;
        }

        this._groupMinWidth = thumbsInGroup * this.thumbnailWidth;
        this._groupMaxWidth = ( thumbsInGroup + 1 ) * this.thumbnailWidth;

        let numGroups = Math.ceil( this._thumbs.length / thumbsInGroup );
        if( numGroups === this._groups.length ) {
            return;
        }

        var galleryGroupTemp = thumbsInGroup;
        var groupDiv = null;
        for ( var i in this._thumbs ) {                    
            var thumbnail = this._thumbs[i];
            if ( galleryGroupTemp === thumbsInGroup ) {
                groupDiv = document.createElement( 'div' );
                groupDiv.classList.add( 'thumbnail-group' );
            }
            groupDiv.appendChild( thumbnail );
            galleryGroupTemp -= 1;
            if ( galleryGroupTemp === 0 || parseInt( i ) === this._thumbs.length - 1 ) {
                galleryGroupTemp = thumbsInGroup;
                this._groups.push( groupDiv );
            }
        }
    }

    _attachGroups() {
        let viewCarousel = this.shadowRoot.querySelector( '#view-carousel' );
        let viewGrid = this.shadowRoot.querySelector( '#view-grid' );
        
        if( viewCarousel ) {
            this.shadowRoot.removeChild( viewCarousel );
        }
        viewCarousel = document.createElement( 'od-carousel' );
        viewCarousel.id = 'view-carousel';
        viewCarousel.setAttribute( 'part', 'view-carousel' );
        viewCarousel.setAttribute( 'exportparts', 'container : view-carousel-inner-container, prev-button : view-carousel-prev-button, next-button : view-carousel-next-button' );

        for( let i = 0; i < this._groups.length; i++ ) {
            let group = this._groups[i];
            if( this.carousel ) {
                viewCarousel.appendChild( group )
            } else {
                viewGrid.appendChild( group )
            }
        }

        this.shadowRoot.appendChild( viewCarousel );
    }

    _reloadGallery() {
        this._removeGalleryGroups();
        this._createGroups();
        this._attachGroups();
    }

    _removeGalleryGroups() {
        let grid = this.shadowRoot.querySelector( '#view-grid' );
        while( grid.childNodes.length > 0 ) {
            grid.removeChild( grid.childNodes[0] );
        }
        this._groups = [];
    }

    _showModal( e ) {
        if ( e && e.type !== 'od-gallery-show-modal' && e.target.id !== 'modal' ) {
            return;
        }
        this.shadowRoot.querySelector( '#modal' ).active = true;
        var imgSrc = e.detail.dataSrc;
        this._setCarousel( imgSrc );
    }

    _setCarousel( selectedSrc ) {                
        var carouselContainer = this.shadowRoot.querySelector( '#modal-carousel-container' );
        var carousel = this.shadowRoot.querySelector( '#modal-carousel' );
        var modalHeight = carouselContainer.offsetHeight;
        var modalWidth = carouselContainer.offsetWidth;

        var modalAspect = modalHeight / modalWidth;

        if ( selectedSrc !== null && selectedSrc !== undefined ) {
            for ( var i in this._images ) {
                var image = this._images[i];
                if ( image.getAttribute( 'src' ) === selectedSrc ) {
                    let imageAspect = image.height / image.width;
                    if( imageAspect > modalAspect ) {
                        image.style.height = '100%';
                        image.style.width = 'auto';
                    } else {                
                        image.style.height = 'auto';
                        image.style.width = '100%';
                    }

                    carousel._selected = image;
                    break;
                }
            }
        }
    }
}
